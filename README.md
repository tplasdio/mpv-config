<p align="center">
  <h1 align="center">📼 mpv-config</h1>
  <p align="center">
    My mpv configurations
  </p>
</p>

## 📰 Description

[Mpv](https://mpv.io/) is a fast simple video player that is nonetheless
extremely customizable and extensible.

These are my personal configurations that make it
even more powerful and awesome.

## 🚀 Installation

Simply copy the files to your mpv configuration directory.

You may first test my configs by putting them in a separate mpv config directory:

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/mpv-config.git
cp -R mpv-config/.config/mpv /home/$USER/.config/mpv2
mpv --config-dir="/home/$USER/.config/mpv2" file
```

## 💻 Scripts

Script|Description
:---:|---
[uosc](https://github.com/tomasklaen/uosc)|Rich, minimalist UI elements
[seek-to.lua](https://github.com/occivink/mpv-scripts#seek-tolua)|Seek to timestamp
[fastforward.lua](https://github.com/zsugabubus/mpv-fastforward)|Increase speed temporarily
[gestures.lua](https://github.com/omeryagmurlu/mpv-gestures)|Touchscreen and mouse gestures
[lrc.lua](https://git.sr.ht/~guidocella/mpv-lrc)|Download lyrics of songs
[thumbfast.lua](https://github.com/po5/thumbfast)|Fast thumbnail generation
shred_file.lua|Shred and remove file on exit (on Linux). Based off [delete_file.lua](https://github.com/zenyd/mpv-scripts/blob/master/delete_file.lua)
[webm.lua](https://github.com/ekisu/mpv-webm)|Simple video editor/cutter that produces webm

## ⌨ Keybindings

*Note. I use a Spanish keyboard and my own custom vim-style bindings `j`,`k`,`l`,`ñ`*

Keybind|Description
:---:|---
`a`|seek to certain timestamp
`j`|fast backwards
`k`|volume up
`l`|volume down
`ñ`|fast forward
`}`|increase speed
`{`|decrease speed
`)`|increase speed temporarily and back to 1x
`BackSpace`|set speed to 1x
`W`|video cutting menu
`e`|next
`w`|previous
`alt j`|pan left
`alt k`|pan up
`alt l`|pan down
`alt ñ`|pan right
`alt +`|zoom
`alt -`|unzoom
`alt r`|rotate
`x`|toggle increasing pitch
`alt m`|download lyrics
`z`|delay lyrics/subtitles
`Z`|undelay lyrics/subtitles
`ctrl o`|save lyrics/subtitles's delay
`Delete`|toggle mark file for shredding

Other keybindings are mostly mpv defaults

## ☝  Touchscreen

Keybind | Description
:---:|---
Slide left|fast backwards
Slide up|volume up
Slide down|volume down
Slide right|fast forward

## 👀 See also

- [mpv User Scripts](https://github.com/mpv-player/mpv/wiki/User-Scripts): More scripts
- [mpv-luasockets](https://codeberg.org/tplasdio/mpv-luasockets): Control multiple mpv instances

## Licence

Visit each script page individually. I don't think my configurations and small changes are copyrightable for now.
